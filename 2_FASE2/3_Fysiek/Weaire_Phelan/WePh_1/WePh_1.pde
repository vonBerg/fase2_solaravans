int verticesCount = 20;
int edgeCount = 30;
int diagonalCount = 160; // 60 buitenkant, 100 binnenkant

// Dodecahedron
PShape dod;
// Tetrakaidecahedron
PShape tet;

boolean isWireFrame = true;

void setup(){
	size(480, 480, P3D);

	dod = createShape();
	dod.beginShape(POINTS);
	dod.scale(10);
	dod.vertex( 3.1498,  0     ,  6.2996);
	dod.vertex(-3.1498,  0     ,  6.2996);
	dod.vertex( 4.1997,  4.1997,  4.1997);
	dod.vertex( 0     ,  6.2996,  3.1498);
	dod.vertex(-4.1997,  4.1997,  4.1997);
	dod.vertex(-4.1997, -4.1997,  4.1997);
	dod.vertex( 0     , -6.2996,  3.1498);
	dod.vertex( 4.1997, -4.1997,  4.1997);
	dod.vertex( 6.2996,  3.1498,  0);
	dod.vertex(-6.2996,  3.1498,  0);
	dod.vertex(-6.2996, -3.1498,  0);
	dod.vertex( 6.2996, -3.1498,  0);
	dod.vertex( 4.1997,  4.1997, -4.1997);
	dod.vertex( 0     ,  6.2996, -3.1498);
	dod.vertex(-4.1997,  4.1997, -4.1997);
	dod.vertex(-4.1997, -4.1997, -4.1997);
	dod.vertex( 0     , -6.2996, -3.1498);
	dod.vertex( 4.1997, -4.1997, -4.1997);
	dod.vertex( 3.1498,  0     , -6.2996);
	dod.vertex(-3.1498,  0     , -6.2996);
	dod.endShape();

	tet = createShape();
	tet.beginShape(POINTS);
	tet.scale(10);
	tet.vertex(	3.14980,  3.70039,  5);
	tet.vertex(-3.14980,  3.70039,  5);
	tet.vertex(-5      ,  0      ,  5);
	tet.vertex(-3.14980, -3.70039,  5);
	tet.vertex( 3.14980, -3.70039,  5);
	tet.vertex( 5      ,  0      ,  5);
	tet.vertex( 4.19974,  5.80026,  0.80026);
	tet.vertex(-4.19974,  5.80026,  0.80026);
	tet.vertex(-6.85020,  0      ,  1.29961);
	tet.vertex(-4.19974, -5.80026,  0.80026);
	tet.vertex( 4.19974, -5.80026,  0.80026);
	tet.vertex( 6.85020,  0      ,  1.29961);
	tet.vertex( 5.80026,  4.19974, -0.80026);
	tet.vertex( 0      ,  6.85020, -1.29961);
	tet.vertex(-5.80026,  4.19974, -0.80026);
	tet.vertex(-5.80026, -4.19974, -0.80026);
	tet.vertex( 0      , -6.85020, -1.29961);
	tet.vertex( 5.80026, -4.19974, -0.80026);
	tet.vertex( 3.70039,  3.14980, -5);
	tet.vertex( 0      ,  5      , -5);
	tet.vertex(-3.70039,  3.14980, -5);
	tet.vertex(-3.70039, -3.14980, -5);
	tet.vertex( 0      , -5      , -5);
	tet.vertex( 3.70039, -3.14980, -5);
	tet.endShape();
}

void draw(){
	background(100);

	fill(255, 0, 0);
	
	lights();

	shape(dod, width/4, height/2);

	shape(tet, width/1.5, height/2);

	// for (int i = 0; i < dod.getVertexCount() - 1; i++) {
	// 	stroke(255,0,0);
	// 	strokeWeight(10);
	// 	line(dod.getVertex(i).x,dod.getVertex(i).y,dod.getVertex(i).z, dod.getVertex(i+1).x,dod.getVertex(i+1).y,dod.getVertex(i+1).z);
	// }
}